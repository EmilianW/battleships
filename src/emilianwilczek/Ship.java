package emilianwilczek;

/*
public enum Ship
{
    AIRCRAFT_CARRIER (5, "Aircraft Carrier"),
    BATTLESHIP (4, "Battleship"),
    CRUISER (3, "Cruiser"),
    DESTROYER (2, "Destroyer"),
    PATROL_SHIP (1, "Patrol Ship");

    private final int size;
    private final String displayName;

    Ship(int size, String displayName)
    {
        this.size = size;
        this.displayName = displayName;
    }

    private int size() { return size; }
    private String displayName() { return displayName; }

    public boolean destroyed = false;
    //enum array[noOfParts]
}
*/

public class Ship
{
    ShipType type;

    //default constructor
    public Ship()
    {
        this.type = ShipType.DESTROYER;
    }

    //constructor if given size
    public Ship(int size)
    {
        switch(size)
        {
            case 1:
                this.type = ShipType.PATROL_SHIP;
                break;
            case 2:
                this.type = ShipType.DESTROYER;
                break;
            case 3:
                this.type = ShipType.CRUISER;
                break;
            case 4:
                this.type = ShipType.BATTLESHIP;
                break;
            case 5:
                this.type = ShipType.AIRCRAFT_CARRIER;
                break;
            default:
                System.out.println("Ship size has to between 1 and 5");
        }
    }

    //constructor if given type
    public Ship(ShipType type)
    {
        switch(type)
        {
            case PATROL_SHIP:
                this.type = ShipType.PATROL_SHIP;
                break;
            case DESTROYER:
                this.type = ShipType.DESTROYER;
                break;
            case CRUISER:
                this.type = ShipType.CRUISER;
                break;
            case BATTLESHIP:
                this.type = ShipType.BATTLESHIP;
                break;
            case AIRCRAFT_CARRIER:
                this.type = ShipType.AIRCRAFT_CARRIER;
                break;
            default:
                System.out.println("Ship type not recognized");
        }
    }

    /* TODO not working with switch statement
    //constructor if given display name
    public Ship(String displayName)
    {
        if(displayName.equals(ShipType.PATROL_SHIP.getDisplayName()))
        {
            this.type = ShipType.PATROL_SHIP;
        }

        switch(displayName)
        {
            case ShipType.PATROL_SHIP.getDisplayName():
                this.type = ShipType.PATROL_SHIP;
                break;
            case ShipType.DESTROYER.getDisplayName():
                this.type = ShipType.DESTROYER;
                break;
            case ShipType.CRUISER.getDisplayName():
                this.type = ShipType.CRUISER;
                break;
            case ShipType.BATTLESHIP.getDisplayName():
                this.type = ShipType.BATTLESHIP;
                break;
            case ShipType.AIRCRAFT_CARRIER.getDisplayName():
                this.type = ShipType.AIRCRAFT_CARRIER;
                break;
            default:
                System.out.println("Ship type not recognized");
        }
    }
     */

    public int getSize()
    {
        return this.type.getSize();
    }

    public ShipType getType()
    {
        return this.type;
    }

    public String getDisplayName()
    {
        return this.type.getDisplayName();
    }


    //constructor if given a type
    //public Ship(Type type)
    /*
    {
        switch(type)
        {
            case PATROL_SHIP:
                this.size = 1;
                this.type = Type.PATROL_SHIP;
                break;
            case DESTROYER:
                this.size = 2;
                this.type = Type.DESTROYER;
                break;
            case CRUISER:
                this.size = 3;
                this.type = Type.CRUISER;
                break;
            case BATTLESHIP:
                this.size = 4;
                this.type = Type.BATTLESHIP;
                break;
            case AIRCRAFT_CARRIER:
                this.size = 5;
                this.type = Type.AIRCRAFT_CARRIER;
                break;
            default:
                System.out.println("This ship type doesn't exist");
                break;
        }
    }
    */
}


