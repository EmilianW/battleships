package emilianwilczek;

public class UI
{
    public void start()
    {
        System.out.println("Hello to Battleships.");
        System.out.println("Do you wish to play? Y/N");

        Coordinate coord = new Coordinate(1);


        SeaGrid grid3 = new SeaGrid();

        grid3.displaySeaGrid();

        Input input = new Input();
        if (input.YesOrNo())
        {
            gameplay();
        }
        else
        {
            System.out.println(" ");
            System.out.println(" ");
            System.out.println(" ");
            System.out.println(" ");
            System.out.println("..oh");
        }
    }

    public static void gameplay()
    {
        //System.out.println("Select a cell to fire at...");
        //SeaGrid seagrid = new SeaGrid(10, 10);

        Ship ship1 = new Ship(1);

        Ship ship2 = new Ship(ShipType.AIRCRAFT_CARRIER);

        System.out.println(ship1.getDisplayName() + "'s type is " + ship1.getType());
        System.out.println(ship1.getDisplayName() + "'s size is " + ship1.getSize());

        System.out.println(ship2.getDisplayName() + "'s type is " + ship2.getType());
        System.out.println(ship2.getDisplayName() + "'s size is " + ship2.getSize());

    }
}
