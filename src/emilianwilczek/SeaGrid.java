package emilianwilczek;

import java.util.Arrays;

public class SeaGrid
{
    SeaGridCell[][] cell;

    final int minGridSize = 1;
    final int maxGridSize = 25;

    /**
     * Default constructor. Creates a blank (not shot at) grid of 10x10.
     */
    public SeaGrid()
    {
        cell = new SeaGridCell[10][10];

        for (SeaGridCell[] y : cell)
            Arrays.fill(y, SeaGridCell.NOT_SHOT_AT);
    }

    /**
     * Constructor if given the size. Creates a blank (not shot at) grid of the specified size.
     *
     * @param sizeX Horizontal number of cells in the grid. Has to be equal or bigger than minGridSize.
     * @param sizeY Vertical number of cells in the grid. Has to be equal or bigger than minGridSize.
     */
    public SeaGrid(int sizeX, int sizeY)
    {
        if (sizeX >= minGridSize && sizeY >= minGridSize && sizeX <= maxGridSize && sizeY <= maxGridSize)
        {
            cell = new SeaGridCell[sizeX][sizeY];
            for (SeaGridCell[] y : cell)
                Arrays.fill(y, SeaGridCell.NOT_SHOT_AT);
        }
        else
        {
            System.out.println("Minimum grid size is " + minGridSize + ". Both sides need to be equal or bigger than that.");
        }
    }

    public void displaySeaGrid()
    {
        System.out.print("   ");
        for(int i = 0; i < cell.length; i++)
        {
            System.out.print((i+1) + "  ");
        }
        System.out.println();
        for(int j = 0; j < cell.length; j++)
        {
            System.out.print("A  ");
            for (SeaGridCell[] seaGridCells : cell)
            {
                if (seaGridCells[j] == SeaGridCell.NOT_SHOT_AT)
                {
                    System.out.print("_  ");
                }
                if (seaGridCells[j] == SeaGridCell.SHOT_AT)
                {
                    System.out.print(" o ");
                }
            }
            System.out.println();
        }
    }
    //populate

    //char or string FormatYCoordsToLetters()
    //coord y from numbers to letters

    //void DisplaySeaGrid()
    //Graphical representation of the sea
    //for coordX.length*coordY.length
    //case !SHOT_AT display blank space
    //case SHOT_AT && !Ship.part display o
    //case SHOT_AT && Ship.part && !Ship.destroyed display x
    //case SHOT_AT && Ship.part && Ship.destroyed display X

    //
    //
}
