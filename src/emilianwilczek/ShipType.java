package emilianwilczek;

public enum ShipType
{
    AIRCRAFT_CARRIER (5, "Aircraft Carrier"),
    BATTLESHIP (4, "Battleship"),
    CRUISER (3, "Cruiser"),
    DESTROYER (2, "Destroyer"),
    PATROL_SHIP (1, "Patrol Ship");

    private final int size;
    private final String displayName;

    ShipType(int size, String displayName)
    {
        this.size = size;
        this.displayName = displayName;
    }

    public int getSize() { return size; }
    public String getDisplayName() { return displayName; }
}
