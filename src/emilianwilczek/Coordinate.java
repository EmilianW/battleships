package emilianwilczek;

public enum Coordinate
{
    A(0),
    B(1),
    C(2),
    D(3),
    E(4),
    F(5),
    G(6),
    H(7),
    I(8),
    J(9),
    K(10),
    L(11),
    M(12),
    N(13),
    O(14),
    P(15),
    R(16),
    S(17),
    T(18),
    U(19),
    V(20),
    W(21),
    X(22),
    Y(23),
    Z(24);

    private final int arrayPosition;

    Coordinate(int arrayPosition)
    {
        this.arrayPosition = arrayPosition;
    }

    public int getArrayPosition() { return arrayPosition; }
}
