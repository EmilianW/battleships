package emilianwilczek;

public enum CardinalDirection
{
    NORTH, EAST, SOUTH, WEST;
}
