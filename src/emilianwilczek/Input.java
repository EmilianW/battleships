package emilianwilczek;

import java.util.Scanner;

public class Input
{
    private final Scanner keyboard = new Scanner(System.in);

    public int Integer()
    {
        while(true)
        {
            String input = keyboard.next();
            try
            {
                return Integer.parseInt(input);
            }
            catch (NumberFormatException nfe)
            {
                System.out.println("Input an integer please.");
            }
        }
    }

    public int Integer(int min, int max)
    {
        while(true)
        {
            String input = keyboard.next();
            try
            {
                int parsedInput = Integer.parseInt(input);
                if (parsedInput >= min && parsedInput <= max)
                {
                    return Integer.parseInt(input);
                }
                else
                {
                    System.out.println("Input an integer between " + min + " and " + max + " please.");
                }
            }
            catch (NumberFormatException nfe)
            {
                System.out.println("Input an integer between " + min + " and " + max + " please.");
            }
        }
    }

    public boolean YesOrNo()
    {
        while(true)
        {
            String input = keyboard.next().toLowerCase();
            switch(input)
            {
                case "y", "yes", "ye", "yeah", "yep": return true;
                case "n", "no": return false;
                default: System.out.println("Input Y/N or Yes/No please.");
            }
        }
    }
}
